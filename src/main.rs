#[cfg(test)]
mod main_tests;

use std::char;

pub fn encrypt_with_trithemius_cipher(string : &str) -> String {
    let mut new_string = String::new();
    let mut shift = 0u32;
    for character in string.chars() {
        let shifted_char = calculate_char_unicode_with_shift(character, shift % 26);
        match char::from_u32(shifted_char) {
            Some(ch) => new_string.push(ch),
            _ => (),
        }
        shift += 1;
    }
    new_string
}


fn calculate_char_unicode_with_shift(character: char, shift: u32) -> u32 {
    if is_letter(character) == false {
        return character as u32;
    }
    let is_small = if character as u32 >= 97 { true } else { false };
    let original_char_to_u32 = character as u32 - if is_small {
        'a' as u32
    } else {
        'A' as u32
    } as u32;
    (original_char_to_u32 + shift) % 26 + if is_small {
        'a' as u32
    } else {
        'A' as u32
    } as u32
}


fn is_letter(character: char) -> bool {
    match character {
        'A'...'Z' | 'a'...'z' => true,
        _ => false
    }
}


fn main() {
    println!("Hello, world!");
}
