use super::*;

#[test] //MACHINE -> MBEKMSK
fn small_big_caps_text_is_encrypted() {
    let small_cap_text = "machine";
    let expected_encrypted = "mbekmsk";
    assert_eq!(expected_encrypted, encrypt_with_trithemius_cipher(small_cap_text));
}


#[test]
fn non_letter_characters_are_not_encrypted() {
    assert_eq!("12 =-/*-", encrypt_with_trithemius_cipher("12 =-/*-"));
}
